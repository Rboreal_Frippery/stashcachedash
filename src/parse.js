$(window).on("load", function(){


    if(window.location.href === "https://ipfs.io/"){
        $('.container').eq(0).prepend('<div id="memes"></div>');
            $('#memes').css({
            'background-image': 'url("https://i.kym-cdn.com/photos/images/newsfeed/000/581/296/c09.jpg")',
            'background-repeat':'no-repeat',
            'background-position': 'center center',
            'content': "",
              'opacity': 0.1,
              'top': 0,
              'left': 0,
              'bottom': 0,
              'right': 0,
              'position': 'absolute'
        })

    }


    var documentClone = document.cloneNode(true);
    var article = new Readability(documentClone).parse();
    if (article !== null){
        article['images'] = [];
        article['videos'] = [];
        var videoElems = $('video');
        console.log(videoElems)

        var getarray = [];



        if(videoElems !== undefined){

            videoElems.each(function(i, v){
                console.log("found video " + this.currentSrc + ", storing along with text article content (main text content likely wrong...)")
                //article
                console.log(this.type)
                getarray.push(videoSrcToData(this.currentSrc, {'type': this}))
                //article['videos'].append({'video_b64':videoSrcToData(this.currentSrc), 'type':this.type});
            })
        }

         $.when.apply($, getarray).then(function(u, out) {
            // do things that need to wait until ALL gets are done
             console.log('---------')
             article['videos'].push(u);
             console.log(article);
             //article['videos'].append()

             chrome.storage.sync.get('settings', function(item) {

                 var parser = document.createElement('a');
                 parser.href = item['settings']['ipfs-host'];
                 const ipfs = new IPFS(parser.hostname, parser.port, parser.protocol.slice(0, parser.protocol.length-1));
                 ipfs.archive(article, function (err, hash) {
                     if(err){
                         console.log("IPFS WRITE ERROR:")
                         console.log(err)
                     }else{
                        console.log(hash)
                         var setting = {};
                         setting[window.location.href] = {'cid': hash, 'dte': new Date().getTime()};
                         chrome.storage.sync.set(setting, function (items) {
                             console.log(items)

                         });
                     }
                 });
             })
        });

        // var imageElems = $('img');
        // if(imageElems !== undefined){
        //
        //     videoElems.each(function(i, v){
        //         console.log("found video " + this.currentSrc + ", storing along with text article content (main text content likely wrong...)")
        //         //article
        //         article['videos'].append(videoSrcToData(this.currentSrc));
        //     })
        // }




    }

})







function videoSrcToData(source_url, obj){
    return $.Deferred(function(dfd_uan) {
        var xhr = new XMLHttpRequest();
        xhr.responseType = 'blob';

        xhr.onload = function () {

            var reader = new FileReader();

            reader.onloadend = function () {

                // var byteCharacters = atob(reader.result.slice(reader.result.indexOf(',') + 1));
                //
                // var byteNumbers = new Array(byteCharacters.length);
                //
                // for (var i = 0; i < byteCharacters.length; i++) {
                //
                //   byteNumbers[i] = byteCharacters.charCodeAt(i);
                //
                // }
                //
                // var byteArray = new Uint8Array(byteNumbers);
                // var blob = new Blob([byteArray], {type: 'video/ogg'});
                // var url = URL.createObjectURL(blob);
                //
                // document.getElementById('_video').src = url;
                obj['video_b64'] = reader.result.slice(reader.result.indexOf(',') + 1);
                dfd_uan.resolve(obj);

            };

            reader.readAsDataURL(xhr.response);

        };

        xhr.open('GET', source_url);
        xhr.send();
    }).promise()
}

