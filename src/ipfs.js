

class IPFS{
    constructor(hostname, port, proto){
        this.ipfs = window.IpfsHttpClient(hostname, port, {'protocol': proto});
    }

    archive(content, callback){
        if(typeof content == "object")
            var data = IpfsHttpClient.Buffer.from(JSON.stringify(content));
        else
            var data = IpfsHttpClient.Buffer.from(content);
        this.ipfs.add(data, (err, res)=>{
            if(res){
                callback(err, res[0].hash);
            }else{
                callback(err, res);
            }
        });
    }

    fetch(hash, callback){
        this.ipfs.get(hash, (err, res) => {
            if (err){
                callback(err, res);
            }else{
                console.log("fetch result");
                console.log(res);
                var string = new TextDecoder("utf-8").decode(res[0].content);
                try{
                    callback(err, JSON.parse(string));
                }catch(err){
                    callback(err, string);
                }

            }
        });
    }

    getUsedSpace(archives, callback){
        var size = 0;
        for(var i = 0; i < archives.length; i++){
            console.log("getting IPFS file with CID:");
            console.log(archives[i].cid);
            if(archives[i].cid != null && archives[i].cid != undefined){
                this.ipfs.ls(archives[i].cid, function(err, files){
                    if(err){
                        console.log("error:");
                        console.log(err);
                    }
                    console.log("in used space callback");
                    console.log(files);
                    if(files && files.length > 0){
                        size += files[0].size;  
                    }

                    
                });
            }
        }
        callback(size);
        
    }
}