function isURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  return !!pattern.test(str);
}

chrome.storage.sync.get(null, function(items) {
    var allKeys = Object.keys(items);

    var settings;

    chrome.storage.sync.get(allKeys, function (items){
        console.log(items)

        // generating settings from prior settings
        if('settings' in items){
            settings = items['settings'];
            delete items['settings'];
        }else{
            settings = {};
        }

        if('ipfs-host' in settings){
            $('#setting-ipfs-host').val(settings['ipfs-host']);
        }
        if('sites-to-archive' in settings){
            if(settings['sites-to-archive'] === true){
                // archive all sites
                $('#settings-all-sites').prop('checked', true);
                $('#sites-to-archive').hide();
            }else{
                $('#settings-all-sites').prop('checked', false);
                $.each(settings['sites-to-archive'], function(i, v){
                    $('#sites-to-archive ul').append('<li><input class="archive-site" value="' + v + '" placeholder="https://...."><button class="archive-site-remove">X</button></li>');
                });

            }
        }else{
            $('#settings-all-sites').prop('checked', true);
            $('#sites-to-archive').hide();
        }


        $('#sites-to-archive ul').append('<li><input class="archive-site" placeholder="https://...."><button class="archive-site-remove">X</button></li>');
        $('#settings-all-sites').change(function(){
            if($(this).prop('checked')){
                $('#sites-to-archive').hide();
            }else{
                $('#sites-to-archive').show();
            }
        });
        $('#add-archive-site').click(function(){
            $('#sites-to-archive ul').append('<li><input class="archive-site" placeholder="https://...."><button class="archive-site-remove">X</button></li>');
        });

        // saving settings
        $('#saveSettingsBtn').click(function(){
            settings['ipfs-host'] = $('#setting-ipfs-host').val();
            if($('#settings-all-sites').prop('checked')){
                settings['sites-to-archive'] = true;
            }else{
                settings['sites-to-archive'] = [];
                $('.archive-site').each(function(i, v){
                    console.log($(this).val())
                    console.log(isURL($(this).val()))
                    if(isURL($(this).val())){
                        settings['sites-to-archive'].push($(this).val());
                    }
                });
                if(settings['sites-to-archive'].length === 0){
                    settings['sites-to-archive'] = true;
                }
            }
            chrome.storage.sync.set({'settings':settings}, function() {
                console.log("Saved new values success");
                location.reload();
            });
        });

        $('.archive-site-remove').click(function(){
            $(this).parent().remove();
        });


        // generating list of archived items
        var all_archived = [];
        for(var key in items){
            items[key]['url'] = key;
            all_archived.push(items[key]);
        };

        all_archived.sort(function(a, b){
            if(a.dte < b.dte) return -1;
            if(a.dte > b.dte) return 1;
            return 0;
        });

        console.log(all_archived)


        $.each(all_archived, function(i, v){
            var row = '<tr>';
            row += '<td>' + String(new Date(v['dte'])) + '</td>';
            row += '<td>' + v['url'] + '</td>';
            row += '<td><a href="' + v['url'] + '">Live Site</a>' +
                '<br><a href="' + 'render.html?url=' + encodeURIComponent(v['url']) + '">My IPFS <img src="icon16.png"> &raquo;</a>' +
                '<br><a href="' + 'https://ipfs.io/ipfs/' + v['cid'] + '">Public IPFS</a></td>';
            row += '</tr>';
            $('#archive-item-table').append(row);
        })
        const ipfs = new IPFS("localhost", 5001);
        ipfs.getUsedSpace(all_archived, (size)=>{
            console.log("size:");
            console.log(size);
            $('#ipfs-size').text("" + size + " B");
        });

        $('#delete-everything').click(function(){
            // does not actually delete everything
            chrome.storage.sync.clear();
            location.reload();
        });

    })

    const imageArr = ["pikachu.png", "thanos.jpg", "zkv5njwvwep21.jpg", "pooh.png"];
    var link = imageArr[Math.floor(Math.random() * imageArr.length)];
    $('#meme').attr('href', link);

});