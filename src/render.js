$(document).ready(function(){

    var params = (new URL(document.location)).searchParams;
    console.log(params.get('url'))

    chrome.storage.sync.get(params.get('url'), function(item) {

        var cid = item[params.get('url')]['cid'];
        console.log(cid)
        chrome.storage.sync.get('settings', function(settings) {

            var parser = document.createElement('a');
            parser.href = settings['settings']['ipfs-host'];

            const ipfs = new IPFS(parser.hostname, parser.port, parser.protocol.slice(0, parser.protocol.length-1));
            ipfs.fetch(cid, function (e, ret) {

                $('title').text('(Archived): ' + ret['title'])
                if (ret['videos'].length > 0 && ret['videos'][0] != null) {
                    make_video(ret['videos'][0]['video_b64']);
                    var content = $(ret['content']);
                    content.find('video').remove();
                    $('#main-content').append(content);
                } else {
                    $('#main-content').html(ret['content'])
                }
            })
        })
    })

    function make_video(video_b64){
        var byteCharacters = atob(video_b64);

        var byteNumbers = new Array(byteCharacters.length);

        for (var i = 0; i < byteCharacters.length; i++) {

          byteNumbers[i] = byteCharacters.charCodeAt(i);

        }

        var byteArray = new Uint8Array(byteNumbers);
        var blob = new Blob([byteArray], {type: 'video'});
        var url = URL.createObjectURL(blob);

        document.getElementById('video-content-0').src = url;
        document.getElementById('video-content-0').style.display = 'block';
    }

})